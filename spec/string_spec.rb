require 'rspec'
require_relative '../model/string'

describe 'String' do
  describe '#length' do
    context 'when string is empty' do
      let(:text) { "" }

      it 'returns 0' do
        expect(text.length).to eq(0)
      end
    end

    context 'when string is not empty' do
      let(:text) { "workshop" }

      it 'does not return 0' do
        expect(text.length).to_not eq(0)
      end
    end
  end

  describe '#downcase' do
    let(:text) { "WORKSHOP" }

    it 'returns "workshop"' do
      expect(text.downcase).to eq("workshop")
    end
  end

  describe '#upcase' do
    let(:text) { "workshop" }

    it 'returns "WORKSHOP"' do
      expect(text.upcase).to eq("WORKSHOP")
    end
  end

  describe '#gsub' do
    let(:text) { "workshop" }

    it 'returns "workfhop"' do
      expect(text.gsub("s", "f")).to eq("workfhop")
    end
  end

  describe '#char_count' do
    context 'when empty text' do
      let(:text) { "" }

      it 'returns {}' do
        expect(text.char_count).to eq({})
      end
    end

    context 'when "aaa"' do
      let(:text) { "aaa" }

      it 'returns {a: 3}' do
        expect(text.char_count).to eq({ a: 3 })
      end
    end

    context 'when "mama"' do
      let(:text) { "mama" }

      it 'returns {m: 2, a: 2}' do
        expect(text.char_count).to eq({ m: 2, a: 2 })
      end
    end

    context 'when mayus' do
      let(:text) { "Mama" }

      it 'returns {m: 2, a: 2}' do
        expect(text.char_count).to eq({ m: 2, a: 2 })
      end
    end

    context 'when special characters' do
      let(:text) { "Mamá" }

      it 'returns {m: 2, a: 2}' do
        expect(text.char_count).to eq({ m: 2, a: 2 })
      end
    end
  end
end
