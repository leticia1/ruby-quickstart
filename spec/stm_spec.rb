require 'rspec'
require 'date'
require_relative '../model/tarjeta'
require_relative '../model/boleto'

describe 'STM' do
  context 'primer boleto' do
    context 'saldo 29' do
      let(:tarjeta) { Tarjeta.new(29, "leti", "comun", Time.new(2018, 02, 24, 12, 0, 0, "+09:00")) }
      let(:boleto)  { Boleto.new("comun", Time.now) }

      before do
        tarjeta.pagar(boleto)
      end

      it 'queda saldo 0' do
        expect(tarjeta.saldo).to eq(0)
      end
    end
  end

  context 'segundo boleto' do
    context 'saldo 29' do
      let(:tarjeta) { Tarjeta.new(29, "leti", "comun", Time.new(2018, 02, 24, 12, 0, 0, "+09:00")) }
      let(:boleto)  { Boleto.new("comun", Time.now) }
      let(:boleto2)  { Boleto.new("comun", Time.now + (60*3)) }

      before do
        tarjeta.pagar(boleto)
        tarjeta.pagar(boleto2)
      end

      it 'queda saldo 0' do
        expect(tarjeta.saldo).to eq(0)
      end
    end
  end
end
