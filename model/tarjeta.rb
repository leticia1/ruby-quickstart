class Tarjeta
  attr_accessor :saldo, :id, :tipo, :limite_gratuito

  def initialize(saldo, id, tipo, limite_gratuito)
    @saldo = saldo
    @id = id
    @tipo = tipo
    @limite_gratuito = limite_gratuito
  end

  def pagar(boleto)
    if @limite_gratuito < boleto.fecha_y_hora
      if boleto.tipo == "comun"
        if @saldo >= 29
          @saldo -= 29
          @limite_gratuito = Time.now + (60*60)
        else
          # raise Error
        end
      end
    end
  end
end
