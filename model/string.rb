class String
  def char_count
    result = {}

    self.gsub!("á", "a")
    self.gsub!("é", "e")
    self.gsub!("í", "i")
    self.gsub!("ó", "o")
    self.gsub!("ú", "u")
    self.gsub!("ü", "u")
    self.gsub!("ñ", "n")

    self.downcase.split('').each do |char|
      if result[char.to_sym]
        result[char.to_sym] += 1
      else
        result[char.to_sym] = 1
      end
    end

    result
  end
end
